# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 15:26:09 2024

@author: ahvca
"""

#%% Import libraries

import pandas as pd


#%% Data handling

def load_excel_data(file_name):
    # Load the Excel file
    xls = pd.ExcelFile(file_name)

    # Initialize a dictionary to store the variables and units
    Variables = {}
    Units = {}

    # Loop through each sheet in the Excel file
    for sheet in xls.sheet_names:
        # Read the sheet into a DataFrame
        data = pd.read_excel(xls, sheet_name=sheet)

        # Check if sheet name contains a space indicating a unit
        if ' ' in sheet:
            variable_name, current_unit = sheet.split(' ', 1)
            current_unit = current_unit.strip('()')  # Remove parentheses
        else:
            variable_name = sheet
            current_unit = None  # No unit found
        
        # Store the units and data in the respective dictionaries
        Units[variable_name] = current_unit
        Variables[variable_name] = data

    return Variables, Units



