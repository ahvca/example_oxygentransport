# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 15:18:54 2024

@author: ahvca
"""


#%% Import libraries

import numpy as np
from scipy.integrate import odeint


#%% Oxygen Transfer Rate (OTR) - ODE Function

def otr(c, t, kla):
    """
    ODE system to calculate oxygen transfer over time.
    :param c: current oxygen concentration (c @ t0 = c0)
    :param t: current time step
    :param kla: volumetric mass transfer coefficient
    :return: dc/dt
    """
    c_max = 100
    dc_dt = kla * (c_max - c)
    return dc_dt


#%% Cost Function for Optimization

def cost(kla, data, time):
    """
    Calculates the cost = Error between model and measurement.
    :param kla: volumetric mass transfer coefficient
    :param data: measured data
    :param time: time points
    :return: J, the sum of squared errors
    """
    c0 = [0]  # Initial concentration for ODE system

    # Solve the ODE
    y = odeint(otr, c0, time, args=(kla,)).flatten()

    # Calculate the sum of squared error
    J = np.sum((y - data) ** 2)

    # Visualize the current model fit to data - uncomment next part
    # plt.figure(2)
    # plt.plot(time, data, label='data')
    # plt.plot(time, y, label='sim')
    # plt.legend()
    # plt.show()

    return J
