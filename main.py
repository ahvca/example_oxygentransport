# -*- coding: utf-8 -*-
"""
Created on Fri Feb  2 15:37:40 2024

@author: ahvca
"""

#%% Import libraries

from data_handling import load_excel_data
from optim_func import cost, otr
import numpy as np
from scipy.optimize import fmin
from scipy.integrate import odeint
import matplotlib.pyplot as plt


#%% Data handling

# Set filename
file_name = 'DO_Reaktor1.xlsx'

# Call the load_data function from data_handling module
variables, units = load_excel_data(file_name)
names = list(variables.keys())


#%% Optimization

# Initialize an array to store the optimized kla values
kla_opt = np.zeros(len(names))

# Set up the figure for a 3x3 grid of subplots
plt.figure(3)
plt.figure(figsize=(10, 10))


# Loop through each dataset
for k, name in enumerate(names):
    kla_0 = 25  # Set an arbitrary start value for kla
    
    # Get current data and time
    data = variables[name]['DO2 (%)'].values
    time = variables[name]['Time (h)'].values
    
    # Start optimization to find kla with minimal cost function value
    kla_opt[k] = fmin(cost, kla_0, args=(data, time), disp=1)
    
    # Solve ODE system for estimated kla (only for visualization needed)
    ysim = odeint(otr, [0], time, args=(kla_opt[k],)).flatten()
    
    # Plotting
    
    ax = plt.subplot(3, 3, k+1)  # Select the subplot where to draw the current dataset
    ax.plot(time, data, label='Data')
    ax.plot(time, ysim, label='Model')
    ax.set_ylabel('DO2 (%)')
    ax.set_xlabel('Time (h)')
    ax.set_ylim([0, 100])
    ax.legend()
    ax.set_title(f'kla = {kla_opt[k]:.2f} h^-1')
    
# Adjust the layout so that subplots do not overlap
plt.tight_layout()
# Display the plots
plt.show()
